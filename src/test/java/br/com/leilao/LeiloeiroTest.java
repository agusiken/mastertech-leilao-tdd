package br.com.leilao;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;

public class LeiloeiroTest {

    @Test
    public void testarMaiorLance(){

        List<Lance> lances = new ArrayList<>();
        Usuario usuario = new Usuario(1, "Usuario 1");

        Lance lance = new Lance(usuario, 1000);
        lances.add(lance);

        lances.add(new Lance(usuario, 10));

        lances.add(new Lance(usuario, 500));

        Leilao leilao = new Leilao(lances);

        Leiloeiro leiloeiro = new Leiloeiro("Leiloeiro", leilao);

        Assertions.assertEquals(leiloeiro.retornarMaiorLance(), lance);
    }
}