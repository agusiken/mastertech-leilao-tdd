package br.com.leilao;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import javax.management.InvalidAttributeValueException;
import java.util.ArrayList;
import java.util.List;

public class LeilaoTest {

    private Lance lance;
    private Usuario usuario;
    private List<Lance> lances;
    private Leilao leilao;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario(1,"Usuario 1");
        lance = new Lance(usuario, 10.00);
        List<Lance> lances = new ArrayList<>();
        lances.add(lance);
        leilao = new Leilao(lances);
    }

    @Test
    public void testarInclusaoDeLance() {
        usuario = new Usuario(2,"Usuario 2");
        lance = new Lance(usuario,20.00);

        Assertions.assertSame(lance, leilao.armazenarLance(lance));
    }

    @Test
    public void testarAdicionarLanceInvalido() {
        Usuario usuario = new Usuario(1, "Usuario 1");
        Lance lance = new Lance(usuario, 19);

        leilao = new Leilao(new ArrayList());

        Assertions.assertThrows(RuntimeException.class, () -> {
            leilao.armazenarLance(lance);
        });
    }
}