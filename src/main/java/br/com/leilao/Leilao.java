package br.com.leilao;

import java.util.List;

public class Leilao {
    private List<Lance> lances;

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public Leilao() {
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public boolean validarLance(Lance lance) {
        boolean resposta = true;
        if (lance.getValorDoLance() <= 0) {
            resposta =  false;
        } else {
            for (Lance lanc : lances) {
                if (lance.getValorDoLance() <= lanc.getValorDoLance()) {
                    resposta = false;
                }
            }
        }
        return resposta;
    }

    public Lance armazenarLance(Lance lance) {
        if (validarLance(lance)) {
            lances.add(lance);
            return lance;
        }
        throw new RuntimeException("Valor não permitido! ");
    }


}
